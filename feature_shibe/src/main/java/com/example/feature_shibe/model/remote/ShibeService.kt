package com.example.feature_shibe.model.remote

import com.example.feature_shibe.model.response.ShibeDTO
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ShibeService {
    companion object {
        private const val BASE_URL = "https://shibe.online"
        private const val QUERY_CATEGORY = "count"

        fun getInstance(): ShibeService {
            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create(ShibeService::class.java)
        }
    }

    @GET("/api/shibes")
    suspend fun getShibes(@Query(QUERY_CATEGORY) number: Int =100): ShibeDTO



}