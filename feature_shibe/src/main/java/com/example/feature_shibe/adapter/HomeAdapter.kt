package com.example.feature_shibe.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.feature_shibe.databinding.ItemHomeBinding
import com.example.feature_shibe.model.ShibeRepo
import com.example.feature_shibe.model.local.entity.Shibe
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeAdapter(val repo: ShibeRepo) :
    RecyclerView.Adapter<HomeAdapter.InputViewHolder>() {

    private var category = mutableListOf<Shibe>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InputViewHolder {
        val binding = ItemHomeBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )

        return InputViewHolder(binding)
    }

    override fun onBindViewHolder(holder: InputViewHolder, position: Int) {
        val category = category[position]
        holder.loadCategory(category, repo)

    }

    override fun getItemCount(): Int {
        return category.size
    }

    fun addCategory(category: List<Shibe>) {
        this.category = category.toMutableList()

    }

    class InputViewHolder(
        private val binding: ItemHomeBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadCategory(shibes: Shibe, repo: ShibeRepo) {
            with(binding) {
                ivFavorite.isVisible = shibes.favoritedShibe
                siShibes.setOnClickListener() {
                    shibes.favoritedShibe = !shibes.favoritedShibe
                    ivFavorite.isVisible = shibes.favoritedShibe
                    CoroutineScope(Dispatchers.IO).launch {
                        repo.shibeDao.update(shibes)
                    }

                }

                Glide.with(siShibes.context).load(shibes.url).into(siShibes)
            }

        }


    }

}