package com.example.feature_shibe.view.category

data class ShibeState(
    val isLoading: Boolean = false,
    val categories: ArrayList<String> = arrayListOf()
)