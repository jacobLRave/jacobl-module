package com.example.feature_bottoms_up.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.bottomsup.model.Rsponse.CategoryDTO
import com.example.feature_bottoms_up.databinding.ItemHomeBinding

class CategoryAdapter(val nav: (String) -> Unit) :
    RecyclerView.Adapter<CategoryAdapter.InputViewHolder>() {

    private var category = mutableListOf<CategoryDTO.CategoryItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InputViewHolder {
        val binding = ItemHomeBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return InputViewHolder(binding)
    }

    override fun onBindViewHolder(holder: InputViewHolder, position: Int) {
        val category = category[position]
        val text = category.strCategory
        holder.loadCategory(text)
        holder.navigateClick().setOnClickListener() {
            nav(text)
        }
    }

    override fun getItemCount(): Int {
        return category.size
    }

    fun addCategory(category: List<CategoryDTO.CategoryItem>) {
        this.category = category.toMutableList()
        notifyDataSetChanged()
    }

    class InputViewHolder(
        private val binding: ItemHomeBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadCategory(category: String) {
            binding.tvContainer.text = category
        }

        fun navigateClick(): TextView {
            return binding.tvContainer
        }

    }

}