package com.example.bottomsup.viewmodel


import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bottomsup.model.BottomsUpRepo
import com.example.bottomsup.model.Rsponse.CategoryDrinksDTO
import com.example.bottomsup.model.Rsponse.DrinkDetailsDTO
import com.example.bottomsup.view.category.CategoryState
import kotlinx.coroutines.launch

class ListViewModel : ViewModel() {

    private val repo by lazy { BottomsUpRepo }

    private val _state = MutableLiveData(CategoryState<CategoryDrinksDTO.Drink>(isLoading = true))
    val state: LiveData<CategoryState<CategoryDrinksDTO.Drink>> get() = _state
    fun getDrinks(type: String) {
        viewModelScope.launch {
            val drinksDTO = repo.getDrinks(type)
            _state.value = CategoryState(categories = drinksDTO.drinks)
        }
    }


}