package com.example.bottomsup.view.category

import com.example.bottomsup.model.Rsponse.CategoryDTO

data class CategoryState<T>(
    val isLoading: Boolean = false,
    val categories: List<T> = emptyList()
)