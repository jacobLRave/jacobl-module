package com.example.bottomsup.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.feature_bottoms_up.adapter.CategoryAdapter
import com.example.bottomsup.viewmodel.CategoryViewModel
import com.example.feature_bottoms_up.databinding.FragmentCategoryBinding

class CategoryFragment : Fragment() {
    private var _binding: FragmentCategoryBinding? = null
    private val binding get() = _binding!!
    private val categoryViewModel by viewModels<CategoryViewModel>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCategoryBinding.inflate(inflater, container, false).also {
        _binding = it

    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        categoryViewModel.state.observe(viewLifecycleOwner) { state ->
            binding.rvList.apply {
                adapter = CategoryAdapter(::navigate).apply {
                    addCategory(state.categories)
                }
            }
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    fun navigate(category: String) {
        findNavController().navigate(
            CategoryFragmentDirections.actionCategoryFragmentToDrinksListFragment(
                category
            )
        )
    }

}