package com.example.bottomsup.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.feature_bottoms_up.adapter.ListAdapter
import com.example.bottomsup.viewmodel.ListViewModel
import com.example.feature_bottoms_up.databinding.FragmentDetailsBinding

class DrinksListFragment : Fragment() {
    private var _binding: FragmentDetailsBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<DrinksListFragmentArgs>()

    private val listViewModel by viewModels<ListViewModel>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDetailsBinding.inflate(inflater, container, false).also {
        _binding = it

    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listViewModel.getDrinks(args.category)
        listViewModel.state.observe(viewLifecycleOwner) { state ->
            binding.rvList.apply {
                adapter = ListAdapter(::navigate).apply {
                    addCategory(state.categories)
                }
            }
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    fun navigate(id: String) {
        findNavController().navigate(
            DrinksListFragmentDirections.actionDrinksListFragmentToDetailsListFragment(
                id
            )
        )
    }

}